# Welcome to my data science repo
This is the place where I keep my small and medium-sized data science projects. Big projects will have their own repos.  
Each folder contains .Rmd file and a compiled .html file.  
The landing page for this repo is also my portfolio website and can be accessed [here](http://bit.ly/2E07nmm).

### Main technologies
- R for EDA and ML
- Python for web scraping

## Projects and descriptions

### House Prices
This is one of my first bigger projects.
The dataset was downloaded from Kaggle. The main point was to learn how to properly fix missing data using different techniques. I have also learned a lot about visualisation and its impacts on understanding the data.
I have used various machine learning algorithms in order to predict the data.

### Lyrics EDA
Web scraping Wikipedia and azlyrics using Python with BeautifulSoup4. The algorithm takes in a URL address of a band's discography's Wikipedia page and finds names, album names and durations of all the songs. Then the algorithm gets the lyrics of each song from azlyrics. The data is exported as a .csv file.  
I have analysed data to find trends, repetitions, most common words, lexical diversity and more using R. I have generated a new The Black Keys song using an LSTM network with Keras.

### Store Item Demand Forecasting (WIP)
In this project, I explore various forecasting methods uni and multivariate datasets. I have prepared the dataset in order to be able to show how the algorithm has predicted the data and how the data actually looked like in a discrete time interval. I am using methods as simple as "simple average" and as complex as "prophet".

## Projects website
Link to my portfolio: [https://tomaszcichy.com/](http://bit.ly/2E07nmm)